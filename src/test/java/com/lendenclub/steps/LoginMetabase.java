package com.lendenclub.steps;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class LoginMetabase {
	WebDriver driver;

	public LoginMetabase(WebDriver driver) {
		this.driver = driver;

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://ldc.lendenclub.com/auth/login/password");

		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys("saurabh.s@lendenclub.com");
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys("saurabh123");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/form/div[4]/button/div/div")).click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[ contains (text(), 'Ask a question' ) ]")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[ contains (text(), 'Native query' ) ]")).click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"DatabasePicker\"]/div/div[2]/div/input")).sendKeys("LenDenClub");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"DatabasePicker\"]/div/div[3]")).click();
		//		driver.findElement(By.xpath("//*[@id=\"DatabasePicker\"]/div/div[11]")).click();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElement(By.id("id_sql")).click();
		driver.findElement(By.className("ace_text-input")).sendKeys(
				"select * from (select lendenapp_requiredloan.user_id,lendenapp_requiredloan.created_date, count(*) over( partition by lendenapp_requiredloan.user_id)  cnt from lendenapp_requiredloan WHERE (lendenapp_requiredloan.status='OPEN' OR lendenapp_requiredloan.status='LOC_OPEN' or lendenapp_requiredloan.status='LOC_LIVE') and (lendenapp_requiredloan.created_date::date >='2022-04-19')) tab where cnt>1 order by user_id");
		//		driver.findElement(By.className("ace_text-input")).sendKeys(
		//				"select lendenapp_requiredloan.user_id, count(lendenapp_requiredloan.user_id) from lendenapp_requiredloan WHERE lendenapp_requiredloan.status='OPEN' OR lendenapp_requiredloan.status='LOC_LIVE' group by lendenapp_requiredloan.user_id having count (lendenapp_requiredloan.user_id)>1");

		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div[2]/div[2]/button")).click();	
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		try {
			driver.get(
					"https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJuYXRpdmUiLCJuYXRpdmUiOnsicXVlcnkiOiJzZWxlY3QgKiBmcm9tIChzZWxlY3QgbGVuZGVuYXBwX3JlcXVpcmVkbG9hbi51c2VyX2lkLGxlbmRlbmFwcF9yZXF1aXJlZGxvYW4uY3JlYXRlZF9kYXRlLCBjb3VudCgqKSBvdmVyKCBwYXJ0aXRpb24gYnkgbGVuZGVuYXBwX3JlcXVpcmVkbG9hbi51c2VyX2lkKSAgY250IGZyb20gbGVuZGVuYXBwX3JlcXVpcmVkbG9hbiBXSEVSRSAobGVuZGVuYXBwX3JlcXVpcmVkbG9hbi5zdGF0dXM9J09QRU4nIE9SIGxlbmRlbmFwcF9yZXF1aXJlZGxvYW4uc3RhdHVzPSdMT0NfT1BFTicgb3IgbGVuZGVuYXBwX3JlcXVpcmVkbG9hbi5zdGF0dXM9J0xPQ19MSVZFJykgYW5kIChsZW5kZW5hcHBfcmVxdWlyZWRsb2FuLmNyZWF0ZWRfZGF0ZTo6ZGF0ZSA-PScyMDIyLTA0LTE5JykpIHRhYiB3aGVyZSBjbnQ-MSBvcmRlciBieSB1c2VyX2lkIiwidGVtcGxhdGUtdGFncyI6e319LCJkYXRhYmFzZSI6Mn0sImRpc3BsYXkiOiJ0YWJsZSIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==");
			//			driver.get(
			//					"https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJuYXRpdmUiLCJuYXRpdmUiOnsicXVlcnkiOiJzZWxlY3QgbGVuZGVuYXBwX3JlcXVpcmVkbG9hbi51c2VyX2lkLCBjb3VudChsZW5kZW5hcHBfcmVxdWlyZWRsb2FuLnVzZXJfaWQpIGZyb20gbGVuZGVuYXBwX3JlcXVpcmVkbG9hbiBXSEVSRSBsZW5kZW5hcHBfcmVxdWlyZWRsb2FuLnN0YXR1cz0nT1BFTicgT1IgbGVuZGVuYXBwX3JlcXVpcmVkbG9hbi5zdGF0dXM9J0xPQ19MSVZFJyBncm91cCBieSBsZW5kZW5hcHBfcmVxdWlyZWRsb2FuLnVzZXJfaWQgaGF2aW5nIGNvdW50IChsZW5kZW5hcHBfcmVxdWlyZWRsb2FuLnVzZXJfaWQpPjEiLCJ0ZW1wbGF0ZS10YWdzIjp7fX0sImRhdGFiYXNlIjo0fSwiZGlzcGxheSI6InRhYmxlIiwidmlzdWFsaXphdGlvbl9zZXR0aW5ncyI6e319");
		} catch (Exception e) {
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().fullscreen();
	}
}
