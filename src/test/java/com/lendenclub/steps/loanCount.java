package com.lendenclub.steps;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class loanCount {
	WebDriver driver;
	boolean test=false;

	public loanCount(WebDriver driver) throws IOException
	{
		this.driver=driver;
		//		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);	
		try {
			WebDriverWait wait = new WebDriverWait(driver, 2);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
		} catch (Exception ex) {
			ex.getMessage();
		}
//		countrow= driver.findElements(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[2]/div/div/div[2]/div/div/div[2]/div/div"));
//					int rowsize=countrow.size();
//					System.out.println("Total Rows is : " + rowsize);
//				System.out.println(countrow);

		String error=null;
		try {
			error=(driver.findElement(By.xpath("//*[ contains (text(), 'No results!' ) ]")).getText().toString());
		} catch(Exception e) {}

		if(error != null)
		{
			
			System.out.println(error);

		} 

		else {
			test = true;
			// Date format for screenshot file name
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			Date date = new Date();
			String fileName = df.format(date);
			File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			// coppy screenshot file into screenshot folder.
			FileUtils.copyFile(file, new File(System.getProperty("user.dir") + "/LoanCount/"+fileName+".jpg"));
		}
		try {
			WebDriverWait wait = new WebDriverWait(driver, 1);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
		} catch (Exception ex) {
			ex.getMessage();
		}
		
		Assert.assertTrue(test, "Data are found");
	}
}
