package com.lendenclub.main;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.lendenclub.selenium.WebCapability;
import com.lendenclub.steps.loanCount;
import com.lendenclub.steps.LoginMetabase;

public class SameUserLoanCount extends WebCapability{

	WebDriver driver;

	@BeforeTest
	public void openingFireFox() throws IOException
	{	
		driver = WebCapability();
	}
	
	@Test(priority = 1)
	public void  loginPage() throws Exception
	{
	  	new LoginMetabase(driver);
	  	
	}
	
	@Test(priority = 2)
	public void  TestLoanProcessingCount() throws Exception
	{
	  	new loanCount(driver);
	  	
	}
	
	@AfterTest
	public void CloseBrowser()
	{
		
	 	driver.quit();
	}


}
