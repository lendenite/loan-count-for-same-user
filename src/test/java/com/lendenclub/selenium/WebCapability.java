package com.lendenclub.selenium;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class WebCapability {
static WebDriver driver;
	
	public static WebDriver WebCapability()
	{
		try {
		
			ChromeOptions opt = new ChromeOptions();
			opt.addArguments("--remote-allow-origins=*");
			WebDriverManager.chromedriver().setup();
			opt.addArguments("no-sandbox");
		    opt.addArguments("headless");
		    opt.addArguments("window-size=1920,1080");
			driver = new ChromeDriver(opt);
			
//			driver.manage().window().fullscreen();
			
			try {
				WebDriverWait wait = new WebDriverWait(driver, 3);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
			} catch (Exception ex) {
				ex.getMessage();
				
			}
			
			System.out.println("Welcome to jenkins");
			
			File source = new File("./LoanCount");
			try {
				FileUtils.cleanDirectory(source);
			
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		
		}
		catch(Exception e){}		
		
		return driver;
	}

}
